"use client";
import React, {FC, useEffect, useState} from "react";
import {Dropdown, DropdownTrigger, DropdownMenu, DropdownItem, Button} from "@nextui-org/react";
import {Link} from "@nextui-org/link";

type KnownForType = {
	knownFor: {
		original_name?: string
		original_title?: string
		poster_path: string
		media_type: string
		id: number
		release_date?: string
		first_air_date?: string
		character?: string
	}[]
}

const FilteredListWorks: FC<KnownForType> = ({knownFor}) => {
	const [filterMedia, setFilterMedia] = useState("all");
	const [filteredList, setFilteredList] = useState(knownFor);
	const copyKnown = knownFor;
	
	const sorted = copyKnown.sort((a, b) => {
		if (a.release_date && b.release_date) {
			return new Date(b.release_date).getTime() - new Date(a.release_date).getTime();
		} else if (a.first_air_date && b.first_air_date) {
			return new Date(b.first_air_date).getTime() - new Date(a.first_air_date).getTime();
		} else {
			return 0;
		}
	});
	[...sorted].forEach((elem, ind) => {
		if (!elem.release_date && !elem.first_air_date) {
			sorted.splice(ind, 1);
			sorted.unshift(elem);
		}
	});
	useEffect(() => {
		if (filterMedia === 'all') {
			setFilteredList(sorted);
		} else {
			setFilteredList(sorted.filter(elem => elem.media_type === filterMedia));
		}
	}, [filterMedia, sorted]);
	return (
		<div className="my-10">
			<div className="flex items-center justify-between mb-3">
				<p className="text-xl">Filtered by: <span className="font-bold">{filterMedia}</span></p>
				<Dropdown>
					<DropdownTrigger>
						<Button variant="bordered">Select type</Button>
					</DropdownTrigger>
					<DropdownMenu
						aria-label="Select media"
						onAction={(key) => setFilterMedia(key as string)}
					>
						<DropdownItem key="all" className={`${filterMedia === "all" ? "bg-gray-400" : ""}`}>All</DropdownItem>
						<DropdownItem key="movie" className={`${filterMedia === "movie" ? "bg-gray-400" : ""}`}>Movies</DropdownItem>
						<DropdownItem key="tv" className={`${filterMedia === "tv" ? "bg-gray-400" : ""}`}>TV</DropdownItem>
					</DropdownMenu>
				</Dropdown>
			</div>
			<ul>
				{ knownFor &&
					filteredList.map((elem, ind) =>
						<li key={`${elem.id}-${elem.media_type}-${ind}`}
							className={`flex border-b border-gray-300 py-2`}>
							<span className="w-[70px]">{(elem.release_date || elem.first_air_date) ?
								(elem.release_date ? elem.release_date.split('-')[0] : elem.first_air_date?.split('-')[0]) : '—'}
							</span>
							<Link className="block text-black"
								href={`/${elem.media_type === "tv" ? "tv" : "movie"}/${elem.original_name ? elem.original_name : elem.original_title}-${elem.id}`}
							>
								<p className="font-bold">{elem.original_name ? elem.original_name : elem.original_title}</p>
								<p><span className="text-gray-500">as</span> {elem.character}</p>
							</Link>
						</li>)
				}
			</ul>
		</div>
	);
};

export default FilteredListWorks;