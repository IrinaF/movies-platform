import Image from "next/image";
import React, {FC} from "react";
import formatDate from "@/utils/formateDate";
import KnownFor from "@/app/person/knownFor";
import FilteredListWorks from "@/app/person/FilteredListWorks";
import SocialMediaLinks from "@/components/social-media-links/social-media-links";
import { knownFor, socialLinks } from "@/types/types";


type PersonDetailsType = {
	personDetails: {
		name: string
		biography: string
		gender: number
		birthday: string
		place_of_birth: string
		known_for_department: string
		also_known_as: string[]
		profile_path: string
	},
	knownFor: knownFor,
	socialLinks: socialLinks
}

const PersonDetails: FC<PersonDetailsType> = ({
												  personDetails: {
													  name,
													  biography,
													  gender,
													  birthday,
													  place_of_birth,
													  known_for_department,
													  also_known_as,
													  profile_path
												  },
												  knownFor,
												  socialLinks
											  }) => {
	return (
		<>
			<div className="flex flex-col sm:flex-row gap-10 mb-10">
				<div className="w-3/12">
					{profile_path ?
						<Image
							src={`https://image.tmdb.org/t/p/w780/${profile_path}`}
							width={780}
							height={140}
							className="w-auto"
							alt="poster-person-img"
						/> : <span className="image-placeholder">📽</span>
					}
					
					<SocialMediaLinks socialLinks={socialLinks} />
					<div>
						<p className="font-bold text-2xl my-4">Personal Info</p>
						<div>
							<p className="font-bold mb-2">Known for: <span
								className="block font-normal">{known_for_department}</span></p>
							<p className="font-bold mb-2">Gender: <span className="block font-normal">{gender === 1 ? 'Female' : 'Male'}</span>
							</p>
							<p className="font-bold mb-2">Birthday: <span
						className="block font-normal">{formatDate(birthday)}</span></p>
							<p className="font-bold mb-2">Place of birth: <span
						className="block font-normal">{place_of_birth}</span></p>
							<p className="font-bold mb-2">Also known as:
								{also_known_as && also_known_as.map(item =>
									<span key={item} className="block font-normal">{item}</span>
								)}
							</p>
						</div>
					</div>
				</div>
				<div className="w-9/12">
					<h3 className="font-bold text-3xl mb-7">{name}</h3>
					<div>
						<p className="font-bold text-xl mb-2">Biography</p>
						<p>{biography}</p>
					</div>
					<KnownFor knownFor={knownFor} />
					<FilteredListWorks knownFor={knownFor} />
				</div>
			</div>
		</>
	);
}

export default PersonDetails;