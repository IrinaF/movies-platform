import findPersonDetails from "@/api/person/findPersonDetails";
import knownForPerson from "@/api/person/knownForPerson";
import externalIds from "@/api/person/externalsIDs";
import PersonDetails from "@/app/person/personDetails";

export default async function PersonPage ({ params }: { params: { personSlug: string } }) {
	const id = params.personSlug.split("-")[1];
	const personDetails = await findPersonDetails(Number(id));
	const knownFor = await knownForPerson(Number(id));
	const socialLinks = await externalIds(Number(id));
	
    return (
		<main className="min-h-screen container mx-auto max-w-7xl py-0 sm:py-24">
			<PersonDetails personDetails={personDetails} knownFor={knownFor.cast} socialLinks={socialLinks} />
		</main>
    );
}