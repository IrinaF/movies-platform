import Image from "next/image";
import React, {FC} from "react";
import Carousel from "@/components/carousel/Сarousel";
import {Link} from "@nextui-org/link";
import { knownFor } from "@/types/types";

type KnownForType = {
	knownFor: knownFor
}

const KnownFor: FC<KnownForType> = ({knownFor}) => {
	return (
		<>
			<p className="font-bold text-xl mb-3 mt-7">Known For</p>
			{knownFor ?
				<Carousel>
					{knownFor.map((item) => (
						<div key={item.id} className="min-h-52 pr-3">
							<Link
								href={`/${item.media_type === "tv" ? "tv" : "movie"}/${item.original_name ? item.original_name : item.original_title}-${item.id}`}>
								{item.poster_path
									? <Image
										src={`https://image.tmdb.org/t/p/w342/${item.poster_path}`}
										width={200}
										height={140}
										className="w-auto rounded-lg"
										alt="recommendation-poster"
									/>
									: <span className="image-placeholder">📽</span>}
							</Link>
							<p className="mt-2 font-bold">{item.original_name ? item.original_name : item.original_title}</p>
						</div>
					))}
				</Carousel>
				: null}
		</>
	);
}

export default KnownFor;