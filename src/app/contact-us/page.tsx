import { FC } from "react";

const ContactUs: FC = () => {
	return (
		<main className="min-h-screen container mx-auto max-w-5xl py-24">
			<h1 className="text-3xl font-bold">Contact Us</h1>
		</main>
	);
};

export default ContactUs;