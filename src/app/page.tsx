"use client";
import { Input } from "@nextui-org/input";
import bg from "../../assets/cinema.webp";
import Tabs from "../components/tabs/Tabs";
import LeaderBoard from "@/components/leader-board/leader-board";
import { Button } from "@nextui-org/react";
import {useEffect, useState} from "react";
import Link from "next/link";
import getTrendiesMovies from "@/api/trendingMovies";
import getPopularTopRatedListList from "@/api/popularTopRatedList";
import {Suspense} from "react";
import Loading from "@/app/loading";


const tabsTrending = [
    { title: "Today", key: "day" },
    { title: "This week", key: "week" },
];
const tabsPopular = [
	{ title: "In Theaters", key: "movie" },
	{ title: "On TV", key: "tv" },
];

const tabsTopRated = [
	{ title: "Movies", key: "movie" },
	{ title: "TV", key: "tv" },
];

const users = [
    { name: "Jane Doe 1", progressAllTime: 40, progressWeek: 50 },
    { name: "Colin Doe", progressAllTime: 10, progressWeek: 20 },
    { name: "Peter", progressAllTime: 67, progressWeek: 15 },
    { name: "Sara", progressAllTime: 70, progressWeek: 30 },
    { name: "Julio", progressAllTime: 34, progressWeek: 12 },
];

export default function Home() {
	const [trendingList, setTrendingList] = useState([]);
	const [popularList, setPopularList] = useState([]);
	const [topRatedList, setTopRatedList] = useState([]);
	const handleTrendingTabChange = async (key: string | number) => {
		const data = await getTrendiesMovies(key as string);
		setTrendingList(data.results);
	};
	
	
	const handlePopularTabChange = async (key: string | number) => {
		const data = await getPopularTopRatedListList(key as string, "popular");
		setPopularList(data.results);
	};
	
	const handleTopRatedTabChange = async (key: string | number) => {
		const data = await getPopularTopRatedListList(key as string, "top_rated");
		setTopRatedList(data.results);
	};
	
	useEffect(() => {
		const fetchLists = async () => {
			const trending = await getTrendiesMovies(tabsTrending[0].key);
			const popular = await getPopularTopRatedListList( tabsPopular[0].key, "popular");
			const topRated = await getPopularTopRatedListList( tabsTopRated[0].key, "top_rated");
			
			setTrendingList(trending.results);
			setPopularList(popular.results);
			setTopRatedList(topRated.results);
		};
		
		fetchLists();
		
	}, []);
	
	const [searchValue, setSearchValue] = useState("") // searchValue
	
    return (
        <>
            <main className="container mx-auto max-w-7xl py-24">
                <div
                    className="flex flex-col justify-center bg-center bg-no-repeat bg-auto bg-slate-500 bg-local min-h-80 max-h-96 px-10 mb-10"
                    style={{ backgroundImage: `url(${bg.src})` }}
                >
                    <h1 className="text-5xl font-bold text-white">Welcome.</h1>
                    <p className="text-3xl font-bold text-white">
                        Millions of movies, TV shows and people to discover.
                        Explore now.
                    </p>
                    <div className="flex relative mt-10">
                        <Input placeholder="Search movie" value={searchValue} onChange={(event) => setSearchValue(event.target.value)} />
                        <Button
                            color="primary"
                            className="absolute right-0">
                            <Link href={`/search/?category=movie&query=${searchValue}`}>Search</Link>
                        </Button>
                    </div>
                </div>
	
				<Suspense fallback={<Loading/>}>
					<Tabs tabs={tabsTrending} onSelectionChange={handleTrendingTabChange} data={trendingList} tabTitle="Trending"/>
					<Tabs tabs={tabsPopular} onSelectionChange={handlePopularTabChange} data={popularList} tabTitle="What's Popular"/>
					<Tabs tabs={tabsTopRated} onSelectionChange={handleTopRatedTabChange} data={topRatedList} tabTitle="Top Rated"/>
				</Suspense>
				
                
                <LeaderBoard users={users} />
            </main>
        </>
    );
}
