import { FC } from "react";

const PrivacyPolicy: FC = () => {
    return (
        <main className="min-h-screen container mx-auto max-w-5xl p-24">
            <div className="mb-10">
                <h1 className="text-3xl font-bold">Privacy Policy</h1>
                <p>
                    This Privacy Policy describes how [Your Company Name]
                    (&quot;Company&quot;, &quot;we&quot;, &quot;us&quot;, or
                    &quot;our&quot;) collects, uses, and discloses your personal
                    information when you use our website or services.
                </p>
            </div>

            <div className="mb-10">
                <h2 className="font-bold">1. Information We Collect</h2>
                <p>
                    We may collect personal information such as your name, email
                    address, mailing address, phone number, and payment
                    information when you use our services.
                </p>
                <p>
                    We also collect information automatically through cookies
                    and similar technologies when you visit our website.
                </p>
            </div>

            <div className="mb-10">
                <h2 className="font-bold">2. How We Use Your Information</h2>
                <p>
                    We use your personal information to provide and improve our
                    services, communicate with you, process payments, and
                    personalize your experience.
                </p>
            </div>

            <div className="mb-10">
                <h2 className="font-bold">
                    3. Information Sharing and Disclosure
                </h2>
                <p>
                    We may share your personal information with third-party
                    service providers who assist us in operating our website or
                    providing our services.
                </p>
                <p>
                    We may also disclose your personal information to comply
                    with applicable laws and regulations or to protect our
                    rights and interests.
                </p>
            </div>

            <div className="mb-10">
                <h2 className="font-bold">4. Data Retention</h2>
                <p>
                    We will retain your personal information for as long as
                    necessary to fulfill the purposes outlined in this Privacy
                    Policy, unless a longer retention period is required or
                    permitted by law.
                </p>
            </div>

            <div className="mb-10">
                <h2 className="font-bold">5. Security</h2>
                <p>
                    We take reasonable measures to protect your personal
                    information from unauthorized access, use, or disclosure.
                </p>
            </div>

            <div className="mb-10">
                <h2 className="font-bold">6. Your Rights</h2>
                <p>
                    You may have the right to access, update, or delete your
                    personal information. Please contact us if you would like to
                    exercise these rights.
                </p>
            </div>

            <div className="mb-10">
                <h2 className="font-bold">7. Changes to This Privacy Policy</h2>
                <p>
                    We may update this Privacy Policy from time to time. We will
                    notify you of any changes by posting the new Privacy Policy
                    on this page.
                </p>
            </div>

            <div className="mb-10">
                <h2 className="font-bold">8. Contact Us</h2>
                <p>
                    If you have any questions about this Privacy Policy, please
                    contact us at [Your Contact Information].
                </p>
            </div>
        </main>
    );
};

export default PrivacyPolicy;
