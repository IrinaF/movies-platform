import SearchContent from "@/app/search/content";
import searchMovie from "@/api/searchByCategory";

export default async function Search({searchParams} : {searchParams: any}) {
	const { query, category } = searchParams;
	const data = await searchMovie(query, category);
	
	return (
		<>
			<SearchContent data={data.results} />
		</>
	);
}

