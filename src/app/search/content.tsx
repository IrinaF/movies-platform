"use client";
import React, {useEffect, useState} from "react";
import {Card, CardHeader, CardBody} from "@nextui-org/card";
import {Link} from "@nextui-org/link";
import { useSearchParams, useRouter} from 'next/navigation';
import MovieCard from "@/components/categories/movie-card";
import TvCard from "@/components/categories/tv-card";
import PeopleCard from "@/components/categories/people-card";
import CollectionCard from "@/components/categories/collection-card";
import KeywordListItem from "@/components/categories/keyword-list-item";
import CompanyItem from "@/components/categories/company-item";
import {Input} from "@nextui-org/react";

const searchCategories: { title: string, value: string }[] = [
	{title: "Movies", value: "movie"},
	{title: "TV Shows", value: "tv"},
	{title: "People", value: "person"},
	{title: "Collections", value: "collection"},
	{title: "Companies", value: "company"},
	{title: "Keywords", value: "keyword"},
	{title: "Networks", value: "network"},
];

export default function SearchContent({data}: {data:any}) {
	const searchParams = useSearchParams();
	const query = searchParams.get('query') || ''
	const router = useRouter()
	const [searchValue, setSearchValue] = useState(query)
	
	useEffect(() => {
		setSearchValue(query);
	}, [query])
	
	const handleOnChange = (event: React.ChangeEvent<HTMLInputElement>) => {
		setSearchValue(event.target.value);
	};
	
	const handleKeydown = (e: React.KeyboardEvent<HTMLInputElement>) => {
		if (e.key === 'Enter') {
			router.push(`/search?category=${searchParams.get('category')}&query=${searchValue}`);
		}
	};
	
	return (
		<>
			<main className="min-h-screen container mx-auto max-w-5xl py-0 sm:py-24">
				
				<Input type="text"
					   variant="underlined"
					   value={searchValue}
					   onKeyDown={(event) => handleKeydown(event)}
					   onChange={handleOnChange}
					   placeholder="Search for a movie, tv show, person..."
					   startContent={<span>🔍</span>}
				/>
				<div className="flex flex-col sm:flex-row gap-10">
					<Card className="min-w-[300px] self-start">
						<CardHeader className="bg-sky-300 text-white py-5">Search Results</CardHeader>
						<CardBody className="p-0 py-3">
							<ul className="flex flex-row sm:flex-col">
								{searchCategories.map((elem) =>
									(
										<li className={`group flex justify-between hover:bg-slate-200  min-w-[150px] pr-3 ${searchParams.get('category') === elem.value ? 'bg-slate-300' : ''}`}
											key={elem.title}>
											<Link
												href={`/search/?category=${elem.value}&query=${searchValue}`}
												className="flex-1 text-left p-3"
											>
												{elem.title}
											</Link>
										</li>))
								}
							</ul>
						</CardBody>
					</Card>
					
					<div className="flex-1">
						{
							data?.length ?
								<ul>
									{data.map((item: any, ind: number) => <li key={ind}>
										{(searchParams.get('category') === 'movie') &&
                                            <MovieCard
                                                posterPath={item.poster_path}
                                                title={item.title}
                                                realeaseDate={item.release_date}
                                                overview={item.overview}
                                                originalTitle={item.original_title}
												id={item.id}
                                            />}
										{(searchParams.get('category') === 'tv') &&
                                            <TvCard
                                                posterPath={item.poster_path}
                                                title={item.name}
                                                realeaseDate={item.release_date}
                                                overview={item.overview}
                                                originalTitle={item.original_name}
                                                id={item.id}
                                            />
										}
										{(searchParams.get('category') === 'collection') &&
                                            <CollectionCard
                                                posterPath={item.poster_path}
                                                name={item.name}
                                                overview={item.overview}
                                                originalName={item.original_name}
                                            />
										}
										{(searchParams.get('category') === 'person') &&
                                            <PeopleCard
                                                profilePath={item.profile_path}
                                                name={item.name}
                                                knownForDepartment={item.known_for_department}
                                                originalName={item.original_name}
                                                knownFor={item.known_for}
                                                id={item.id}
                                            />
										}
										{(searchParams.get('category') === 'keyword') &&
                                            <KeywordListItem id={item.id} name={item.name} />
										}
										{(searchParams.get('category') === 'company') &&
                                            <CompanyItem
                                                id={item.id}
                                                name={item.name}
                                                logoPath={item.logo_path}
                                                originCountry={item.origin_country}
                                            />
										}
									</li>)}
								</ul> : "No results"
						}
					</div>
					
				</div>
				
			
			</main>
		</>
	);
}

