import Link from "next/link";
export default function About() {
    return (
        <main className="container mx-auto max-w-5xl min-h-screen py-24">
            <h1>About page</h1>
            <p><Link href="/about/about-1">About 1</Link></p>
            <p><Link href="/about/about-2">About 2</Link></p>
        </main>
    );
}
