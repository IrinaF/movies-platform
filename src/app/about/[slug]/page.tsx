export default function AboutPageDynamic ({ params }: { params: { slug: string } }) {
    return (
        <main className="min-h-screen p-24">
            <h1>Dynamic page {params.slug}</h1>
        </main>
    );
}