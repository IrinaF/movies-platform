import findTvDetails from "@/api/tv/findTvDetails";
import tvCast from "@/api/tv/tvCast";
import tvKeywords from "@/api/tv/tvKeywords";
import tvRecommendations from "@/api/tv/tvRecommendations";
import tvReviews from "@/api/tv/tvReviews";
import externalIds from "@/api/tv/externalsIDs";
import TvDetails from "@/app/tv/tvDetails";
import TvCast from "@/app/tv/tvCast";
import TvKeywords from "@/app/tv/tvKeywords";
import TvAddDetails from "@/app/tv/tvAddDetails";
import TvRecommendations from "@/app/tv/tvRecommendations";
import TvReview from "@/app/tv/tvReview";
import SocialMediaLinks from "@/components/social-media-links/social-media-links";

export default async function TvPage ({ params }: { params: { tvSlug: string } }) {
	const id = params.tvSlug.split("-")[1];
	const tvDetails = await findTvDetails(Number(id));
	const tvCastList = await tvCast(Number(id));
	const tvKeywordsList = await tvKeywords(Number(id));
	const tvRecommendationsList = await tvRecommendations(Number(id));
	const tvReviewsList = await tvReviews(Number(id));
	const socialLinks = await externalIds(Number(id));
    return (
		<main className="min-h-screen container mx-auto max-w-7xl py-0 sm:py-24">
			<TvDetails tvDetails={tvDetails} />
			<div className="flex flex-col sm:flex-row gap-10">
				<div className="w-10/12">
					<TvCast tvCastList={tvCastList} />
					{ (tvReviewsList.results && tvReviewsList.results.length > 0) ? <TvReview tvReviews={tvReviewsList.results[0]} link={`/tv/${params.tvSlug}/reviews`} /> : <p>We do&lsquo;nt have any reviews</p> }
					<TvRecommendations recommendations={tvRecommendationsList.results} />
				</div>
				<div className="w-2/12">
					<SocialMediaLinks socialLinks={socialLinks} />
					<TvAddDetails tvDetails={tvDetails} />
					<TvKeywords tvKeywords={tvKeywordsList.results} />
				</div>
			</div>
		</main>
		
    );
}