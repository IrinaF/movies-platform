import React, {FC} from "react";

type TvAddDetailsType = {
	tvDetails: {
		status: string;
		original_language: string;
		type: string;
		networks: {
			name: string
		}[]
	}
}
const TvAddDetails: FC<TvAddDetailsType> = ({tvDetails}) => {
	return (
		<>
			<div className={"mb-3"}>
				<p className={"font-bold"}>Status</p>
				<p>{tvDetails.status}</p>
			</div>
			<div className={"mb-3"}>
				<p className={"font-bold"}>Original Language</p>
				<p>{tvDetails.original_language}</p>
			</div>
			<div className={"mb-3"}>
				<p className={"font-bold"}>Type</p>
				<p>{tvDetails.type}</p>
			</div>
			<div className={"mb-3"}>
				<p className={"font-bold"}>Networks</p>
				<p>{tvDetails.networks && tvDetails.networks.map(elem => elem.name)}</p>
			</div>
		</>
	)
}

export default TvAddDetails;