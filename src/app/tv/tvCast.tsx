import Image from "next/image";
import React, {FC} from "react";
import Carousel from "@/components/carousel/Сarousel";
import {Link} from "@nextui-org/link";

type TvCastType = {
	tvCastList: {
		cast: {
			name: string;
			profile_path: string;
			total_episode_count: string;
			id: number;
		}[]
	}
}

const TvCast: FC<TvCastType> = ({tvCastList}) => {
	return (
		<>
			<p className="font-bold text-xl mb-3 mt-3">Cast</p>
			{ tvCastList.cast ?
				<Carousel>
					{tvCastList.cast.map((item: any) => (
						<div key={item.id} className="min-h-52 pr-3">
							<Link href={`/person/${item.name}-${item.id}`}>
								{item.profile_path
									? <Image
										src={`https://image.tmdb.org/t/p/w342${item.profile_path}`}
										width={95}
										height={140}
										className="w-auto rounded-lg"
										alt="cast-poster"
									/>
									: <span className="image-placeholder">📽</span>}
							</Link>
							<p className="font-bold mt-2">{item.name}</p>
							<p className="text-sm">{item.total_episode_count} {item.total_episode_count > 1 ? 'Episodes' : 'Episode'}</p>
						</div>
					))}
				</Carousel> : null}
		</>
	);
}

export default TvCast;