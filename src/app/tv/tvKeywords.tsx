import React, {FC} from "react";

const TvKeywords: FC<{tvKeywords: {name: string, id: number}[]}> = ({ tvKeywords }) => {
	return (
		<>
			<p className="font-bold text-xl mb-3">Keywords</p>
			<div className="flex gap-2 flex-wrap">
				{tvKeywords && tvKeywords.map((item: any) => (
					<span className="bg-slate-400 px-2 rounded text-white text-xs" key={item.id}>{item.name}</span>
				))}
			</div>
		</>
	);
}

export default TvKeywords;