import Image from "next/image";
import React, {FC} from "react";
import {CircularProgress} from "@nextui-org/react";

type TvDetailsType = {
	tvDetails: {
		poster_path: string;
		name: string;
		first_air_date: string;
		overview: string;
		genres: { name: string }[];
		vote_average: number;
		runtime: number;
		original_name: string;
		tagline: string;
	}
}

const TvDetails: FC<TvDetailsType> = ({tvDetails}) => {
	return (
		<>
			<div className="flex flex-col sm:flex-row gap-10 mb-10">
				{tvDetails.poster_path ?
					<Image
						src={`https://image.tmdb.org/t/p/w780${tvDetails.poster_path}`}
						width={780}
						height={140}
						className="w-auto"
						alt="poster-movie-img"
					/> : <span className="image-placeholder">📽</span>
				}
				
				<div>
					<h2 className="font-bold text-2xl">{tvDetails.original_name}
						<span>({tvDetails.first_air_date && tvDetails.first_air_date.split('-')[0]})</span></h2>
					<p className="text-slate-500">
						{tvDetails.first_air_date} <span> | </span>
						{tvDetails.genres && tvDetails.genres.map(item => item.name).join(', ')} <span> | </span>
						{tvDetails.runtime ? `${tvDetails.runtime} min` : ''}
					</p>
					<CircularProgress
						aria-label="User rating"
						size="lg"
						value={tvDetails.vote_average * 10}
						color="warning"
						className="bg-black rounded-full text-white mt-5"
						strokeWidth={3}
						showValueLabel={true}
					/>
					<div className="mt-5">
						<p className="text-slate-500 font-bold mb-2">{tvDetails.tagline}</p>
						<p className="font-bold">Overview</p>
						<p>{tvDetails.overview}</p>
					</div>
				</div>
			</div>
		</>
	);
}

export default TvDetails;