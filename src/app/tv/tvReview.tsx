"use client";
import React, {FC, useState, useRef, useEffect} from 'react';
import {Card, CardBody} from "@nextui-org/react";
import Link from "next/link";
import {Button} from "@nextui-org/react";
import {Avatar} from "@nextui-org/avatar";
import formatDate from "@/utils/formateDate";

type TvReviewProps = {
	tvReviews: {
		id: number
		author: string
		content: string
		author_details: {
			avatar_path: string
		}
		created_at: string
	}
	link: string
}
const TvReview: FC<TvReviewProps> = ({tvReviews, link}: TvReviewProps) => {
	const [showMore, setShowMore] = useState(false);
	const [showMoreBtn, setShowMoreBtn] = useState(false);
	const contentRef = useRef<HTMLDivElement>(null);

	useEffect(() => {
		if (contentRef.current) {
			if (contentRef.current.scrollHeight > contentRef.current.clientHeight && !showMore) {
				setShowMoreBtn(true);
			} else {
				setShowMoreBtn(false);
			}
		}
	}, [contentRef, showMore]);
	
	return (
		<>
			<div className="flex items-center mb-3 mt-10">
				<p className="font-bold text-xl mr-5">Reviews</p>
				<Link href={link}>
					<Button variant="bordered" color="primary" size="sm">Read All Reviews</Button>
				</Link>
			</div>
			<Card key={tvReviews.id}>
				<CardBody className="">
					<div className="flex items-center mb-3 gap-3">
						<Avatar
							name={tvReviews.author}
							src={`https://image.tmdb.org/t/p/w342${tvReviews.author_details.avatar_path}`}
							size="lg"
						/>
						<div>
							<p className="text-xl font-bold">A review by {tvReviews.author}</p>
							<p className="text-sm text-slate-500">
								Written by
								<span className="font-bold mx-1">{tvReviews.author}</span>
								on {formatDate(tvReviews.created_at)}
							</p>
						</div>
					</div>
					<p ref={contentRef} className={`text-lg max-h-[200px] overflow-hidden ${showMore ? 'max-h-full' : ''}`}>{tvReviews.content}</p>
					{/*<Button color="primary" variant="light" className="w-16 mt-2" onClick={() => setShowMore(!showMore)}>See more</Button>*/}
					{ showMoreBtn ? <Button color="primary" variant="light" className="w-16 mt-2" onClick={() => setShowMore(!showMore)}>See more</Button> : null }
				</CardBody>
			</Card>
		</>
	)
};
export default TvReview;