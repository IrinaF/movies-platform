import { FC } from "react";

const TermsOfService: FC = () => {
    return (
        <main className="min-h-screen container mx-auto max-w-5xl p-24">
            <div className="mb-10">
                <h1 className="text-3xl font-bold">Terms of Service</h1>
                <p>
                    This Terms of Service agreement (&quot;Agreement&quot;)
                    governs your use of [Your Pet Project] (&quot;Service&quot;)
                    provided by [Your Company Name] (&quot;Company&quot;,
                    &quot;we&quot;, &quot;us&quot;, or &quot;our&quot;). By
                    accessing or using the Service, you agree to be bound by
                    this Agreement.
                </p>
            </div>

            <div className="mb-10">
                <h2 className="font-bold">1. Acceptance of Terms</h2>
                <p>
                    By accessing or using the Service, you agree to be bound by
                    these Terms. If you do not agree to these Terms, you may not
                    use the Service.
                </p>
            </div>

            <div className="mb-10">
                <h2 className="font-bold">2. Use of the Service</h2>
                <p>
                    a. <strong>Eligibility:</strong> You must be at least 18
                    years old to use the Service. By using the Service, you
                    represent and warrant that you are at least 18 years old.
                </p>
                <p>
                    b. <strong>License:</strong> We grant you a limited,
                    non-exclusive, non-transferable license to use the Service
                    for personal or internal business purposes.
                </p>
                <p>
                    c. <strong>Prohibited Conduct:</strong> You agree not to
                    engage in any of the following prohibited activities: (i)
                    copying, distributing, or disclosing any part of the Service
                    in any medium; (ii) using any automated system, including
                    &quot;bots,&quot; &quot;spiders,&quot; or
                    &quot;crawlers,&quot; to access or interact with the
                    Service; (iii) attempting to interfere with, compromise the
                    system integrity or security, or decipher any transmissions
                    to or from the servers running the Service.
                </p>
            </div>

            <div className="mb-10">
                <h2 className="font-bold">3. Intellectual Property</h2>
                <p>
                    All content, trademarks, service marks, and logos provided
                    through the Service are owned by or licensed to the Company
                    and are subject to copyright and other intellectual property
                    rights under the law.
                </p>
            </div>

            <div className="mb-10">
                <h2 className="font-bold">4. Privacy</h2>
                <p>
                    Your privacy is important to us. Our Privacy Policy governs
                    the collection and use of your information. By using the
                    Service, you consent to the terms of our Privacy Policy.
                </p>
            </div>

            <div className="mb-10">
                <h2 className="font-bold">5. Disclaimer of Warranties</h2>
                <p>
                    The Service is provided &quot;as is&quot; and &quot;as
                    available&quot; without warranties of any kind, either
                    express or implied, including, but not limited to, implied
                    warranties of merchantability, fitness for a particular
                    purpose, or non-infringement.
                </p>
            </div>

            <div className="mb-10">
                <h2 className="font-bold">6. Limitation of Liability</h2>
                <p>
                    In no event shall the Company, its officers, directors,
                    employees, or agents be liable for any indirect, incidental,
                    special, consequential, or punitive damages, including
                    without limitation, loss of profits, data, use, goodwill, or
                    other intangible losses, arising out of or in connection
                    with your access to or use of the Service.
                </p>
            </div>

            <div className="mb-10">
                <h2 className="font-bold">7. Governing Law</h2>
                <p>
                    This Agreement shall be governed by and construed in
                    accordance with the laws of [Your Jurisdiction], without
                    regard to its conflict of law principles.
                </p>
            </div>

            <div className="mb-10">
                <h2 className="font-bold">8. Changes to Terms</h2>
                <p>
                    We reserve the right to modify or replace these Terms at any
                    time. If a revision is material, we will provide at least 30
                    days notice prior to any new terms taking effect.
                </p>
            </div>

            <div className="mb-10">
                <h2 className="font-bold">9. Contact Us</h2>
                <p>
                    If you have any questions about these Terms, please contacts
                    us at [TBD].
                </p>
            </div>
        </main>
    );
};

export default TermsOfService;
