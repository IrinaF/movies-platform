import Image from "next/image";
import React, {FC} from "react";
import {CircularProgress} from "@nextui-org/react";

type MovieDetailsType = {
	movieDetails: {
		poster_path: string;
		title: string;
		release_date: string;
		overview: string;
		genres: { name: string }[];
		vote_average: number;
		runtime: number;
		original_title: string;
		tagline: string;
	}
}

const MovieDetails: FC<MovieDetailsType> = ({movieDetails}) => {
	return (
		<>
			<div className="flex flex-col sm:flex-row gap-10 mb-10">
				{movieDetails.poster_path ?
                    <Image
                        src={`https://image.tmdb.org/t/p/w780/${movieDetails.poster_path}`}
                        width={780}
                        height={140}
                        className="w-auto"
                        alt="poster-movie-img"
                    /> : <span className="image-placeholder">📽</span>
				}
				<div>
					<h2 className="font-bold text-2xl">{movieDetails.title ? movieDetails.title : movieDetails.original_title}
						<span>({movieDetails.release_date && movieDetails.release_date.split('-')[0]})</span></h2>
					<p className="text-slate-500">
						{movieDetails.release_date} <span> | </span>
						{movieDetails.genres && movieDetails.genres.map(item => item.name).join(', ')} <span> | </span>
						{movieDetails.runtime ? `${movieDetails.runtime} min` : ''}
					</p>
					<CircularProgress
						aria-label="User rating"
						size="lg"
						value={movieDetails.vote_average * 10}
						color="warning"
						className="bg-black rounded-full text-white mt-5"
						strokeWidth={3}
						showValueLabel={true}
					/>
					<div className="mt-5">
						<p className="text-slate-500 font-bold mb-2">{movieDetails.tagline}</p>
						<p className="font-bold">Overview</p>
						<p>{movieDetails.overview}</p>
					</div>
				</div>
			</div>
		</>
	);
}

export default MovieDetails;