"use client";
import React, {FC, useState, useRef, useEffect} from 'react';
import {Card, CardBody} from "@nextui-org/react";
import Link from "next/link";
import {Button} from "@nextui-org/react";
import {Avatar} from "@nextui-org/avatar";
import formatDate from "@/utils/formateDate";

type MovieReviewProps = {
	movieReviews: {
		id: number
		author: string
		content: string
		author_details: {
			avatar_path: string
		}
		created_at: string
	}
	link: string
}
const MovieReview: FC<MovieReviewProps> = ({movieReviews, link}: MovieReviewProps) => {
	const [showMore, setShowMore] = useState(false);
	const [showMoreBtn, setShowMoreBtn] = useState(false);
	const contentRef = useRef<HTMLDivElement>(null);

	useEffect(() => {
		if (contentRef.current) {
			if (contentRef.current.scrollHeight > contentRef.current.clientHeight && !showMore) {
				setShowMoreBtn(true);
			} else {
				setShowMoreBtn(false);
			}
		}
	}, [contentRef, showMore]);
	
	return (
		<>
			<div className="flex items-center mb-3 mt-10">
				<p className="font-bold text-xl mr-5">Reviews</p>
				<Link href={link}>
					<Button variant="bordered" color="primary" size="sm">Read All Reviews</Button>
				</Link>
			</div>
			<Card key={movieReviews.id}>
				<CardBody className="">
					<div className="flex items-center mb-3 gap-3">
						<Avatar
							name={movieReviews.author}
							src={`https://image.tmdb.org/t/p/w342${movieReviews.author_details.avatar_path}`}
							size="lg"
						/>
						<div>
							<p className="text-xl font-bold">A review by {movieReviews.author}</p>
							<p className="text-sm text-slate-500">
								Written by
								<span className="font-bold mx-1">{movieReviews.author}</span>
								on {formatDate(movieReviews.created_at)}
							</p>
						</div>
					</div>
					<p ref={contentRef} className={`text-lg max-h-[200px] overflow-hidden ${showMore ? 'max-h-full' : ''}`}>{movieReviews.content}</p>
					{ showMoreBtn ? <Button color="primary" variant="light" className="w-16 mt-2" onClick={() => setShowMore(!showMore)}>See more</Button> : null }
				</CardBody>
			</Card>
		</>
	)
};
export default MovieReview;