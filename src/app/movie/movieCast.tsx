import Image from "next/image";
import React, {FC} from "react";
import Carousel from "@/components/carousel/Сarousel";
import {Link} from "@nextui-org/link";

type MovieCastType = {
	movieCastList: {
		cast: {
			name: string;
			profile_path: string;
			character: string;
			id: number;
		}[]
	}
}

const MovieCast: FC<MovieCastType> = ({movieCastList}) => {
	return (
		<>
			<p className="font-bold text-xl mb-3 mt-3">Cast</p>
			{ movieCastList.cast ?
				<Carousel>
					{movieCastList.cast.map((item: any) => (
						<div key={item.id} className="min-h-52 pr-3">
							<Link href={`/person/${item.name}-${item.id}`}>
								{item.profile_path
									? <Image
										src={`https://image.tmdb.org/t/p/w342${item.profile_path}`}
										width={95}
										height={140}
										className="w-auto rounded-lg"
										alt="cast-poster"
									/>
									: <span className="image-placeholder">📽</span>}
							</Link>
							<p className="font-bold mt-2">{item.name}</p>
							<p className="text-sm">{item.character}</p>
						</div>
					))}
				</Carousel> : null}
		</>
	);
}

export default MovieCast;