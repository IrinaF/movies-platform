import React, {FC} from "react";

type MovieAddDetailsType = {
	movieDetails: {
		status: string;
		original_language: string;
		budget: number;
		revenue: number;
	}
}
const movieAddDetails: FC<MovieAddDetailsType> = ({movieDetails}) => {
	return (
		<>
			<div className={"mb-3"}>
				<p className={"font-bold"}>Status</p>
				<p>{movieDetails.status}</p>
			</div>
			<div className={"mb-3"}>
				<p className={"font-bold"}>Original Language</p>
				<p>{movieDetails.original_language}</p>
			</div>
			<div className={"mb-3"}>
				<p className={"font-bold"}>Budget</p>
				<p>{movieDetails.budget ?
					`$${movieDetails.budget.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",")}`
					: '-'
				}</p>
			</div>
			<div className={"mb-3"}>
				<p className={"font-bold"}>Revenue</p>
				<p>{movieDetails.revenue ?
					`$${movieDetails.revenue.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",")}`
					: '-'}
				</p>
			</div>
		</>
	)
}

export default movieAddDetails;