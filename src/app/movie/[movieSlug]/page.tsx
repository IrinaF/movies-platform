import findMovieDetails from "@/api/movies/findMovieDetails";
import movieCast from "@/api/movies/movieCast";
import movieKeywords from "@/api/movies/movieKeywords";
import movieRecommendations from "@/api/movies/movieRecommendations";
import movieReviews from "@/api/movies/movieReviews";
import externalIds from "@/api/movies/externalsIDs";
import MovieDetails from "@/app/movie/movieDetails";
import MovieCast from "@/app/movie/movieCast";
import MovieKeywords from "@/app/movie/movieKeywords";
import MovieAddDetails from "@/app/movie/movieAddDetails";
import MovieRecommendations from "@/app/movie/movieRecommendations";
import MovieReview from "@/app/movie/movieReview";
import SocialMediaLinks from "@/components/social-media-links/social-media-links";

export default async function MoviePage ({ params }: { params: { movieSlug: string } }) {
	const id = params.movieSlug.split("-")[1];
	const movieDetails = await findMovieDetails(Number(id));
	const movieCastList = await movieCast(Number(id));
	const movieKeywordsList = await movieKeywords(Number(id));
	const movieRecommendationsList = await movieRecommendations(Number(id));
	const movieReviewsList = await movieReviews(Number(id));
	const socialLinks = await externalIds(Number(id));
    return (
		<main className="min-h-screen container mx-auto max-w-7xl py-0 sm:py-24">
			<MovieDetails movieDetails={movieDetails} />
			<div className="flex flex-col sm:flex-row gap-10">
				<div className="w-10/12">
					<MovieCast movieCastList={movieCastList} />
					{(movieReviewsList.results && movieReviewsList.results[0] ) && <MovieReview movieReviews={movieReviewsList.results[0]} link={`/movie/${params.movieSlug}/reviews`} />}
					<MovieRecommendations recommendations={movieRecommendationsList.results} />
				</div>
				<div className="w-2/12">
					<SocialMediaLinks socialLinks={socialLinks} />
					<MovieAddDetails movieDetails={movieDetails} />
					<MovieKeywords movieKeywords={movieKeywordsList.keywords} />
				</div>
			</div>
		</main>
		
    );
}