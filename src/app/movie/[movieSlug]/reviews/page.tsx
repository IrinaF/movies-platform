import {Card, CardBody} from "@nextui-org/react";
import movieReviews from "@/api/movies/movieReviews";
import {Avatar} from "@nextui-org/avatar";
import formatDate from "@/utils/formateDate";
import React from "react";

export default async function MovieReviewsAll ({ params }: { params: { movieSlug: string } }) {
	const id = params.movieSlug.split("-")[1];
	const movieReviewsList = await movieReviews(Number(id));
	
	return (
		<main className="min-h-screen container mx-auto max-w-5xl py-24">
			{movieReviewsList.results.map((elem: any) => (
				<Card key={elem.id} className="mb-10">
					<CardBody>
						<div className="flex items-center mb-3 gap-3">
							<Avatar
								name={elem.author}
								src={`https://image.tmdb.org/t/p/w342${elem.author_details.avatar_path}`}
								size="lg"
							/>
							<div>
								<p className="text-xl font-bold">A review by {elem.author}</p>
								<p className="text-sm text-slate-500">
									Written by
									<span className="font-bold mx-1">{elem.author}</span>
									on {formatDate(elem.created_at)}
								</p>
							</div>
						</div>
						<p className="text-lg">{elem.content}</p>
					</CardBody>
				</Card>
				))
			}
		</main>
	)
}