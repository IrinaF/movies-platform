import Image from "next/image";
import {Link} from "@nextui-org/link";
import React, {FC} from "react";
import Carousel from "@/components/carousel/Сarousel";

type MovieRecommendationsType = {
	recommendations: {
		poster_path: string;
		original_title: string;
		vote_average: number;
		id: number;
	}[]
}

const MovieRecommendations: FC<MovieRecommendationsType> = ({recommendations}) => {
	return (
		<>
			<p className="font-bold text-xl mb-3 mt-10">Recommendations</p>
			{recommendations &&
				<Carousel>
					{recommendations.map((item: any) => (
						<div key={item.id} className="min-h-52 pr-3">
							<Link className="flex-none" href={`/movie/${item.title}-${item.id}`}>
								{item.poster_path
									? <Image
										src={`https://image.tmdb.org/t/p/w342${item.poster_path}`}
										width={200}
										height={140}
										className="w-auto rounded-lg"
										alt="recommendation-poster"
									/>
									: <span className="image-placeholder">📽</span>}
							</Link>
							<div className="flex max-w-[170px] mt-2 justify-between items-center gap-2">
								<p className="leading-none text-sm text-slate-500">{item.original_title}</p>
								<p className="text-sm text-slate-500 font-bold">{`${Math.floor(item.vote_average * 10)}%`}</p>
							</div>
						</div>
					))}
			</Carousel>}
		</>
	);
};

export default MovieRecommendations