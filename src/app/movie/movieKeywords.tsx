import React, {FC} from "react";

const MovieKeywords: FC<{movieKeywords: {name: string, id: number}[]}> = ({ movieKeywords }) => {
	return (
		<>
			<p className="font-bold text-xl mb-3">Keywords</p>
			<div className="flex gap-2 flex-wrap">
				{movieKeywords &&movieKeywords.map((item: any) => (
					<span className="bg-slate-400 px-2 rounded text-white text-xs" key={item.id}>{item.name}</span>
				))}
			</div>
		</>
	);
}

export default MovieKeywords;