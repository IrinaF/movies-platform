export type knownFor = {
	original_name?: string
	original_title?: string
	poster_path: string
	media_type: string
	id: number
}[];

export type socialLinks = {
	imdb_id: string
	instagram_id: string
	twitter_id: string
	facebook_id: string
	youtube_id: string
	tiktok_id: string
};