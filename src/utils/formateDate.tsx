export default function formatDate(isoDateString: string) {
	const date = new Date(isoDateString);
	
	const options: { [key: string]: string | number } = { year: 'numeric', month: 'long', day: 'numeric' };
	return date.toLocaleDateString('en-US', options);
}