const getTrendiesMovies = async (timeWindow: string) => {
	const url = `https://api.themoviedb.org/3/trending/all/${timeWindow}?language=en-US`;
	const options = {
		method: 'GET',
		headers: {
			accept: 'application/json',
			Authorization: `Bearer ${process.env.NEXT_PUBLIC_API_BEARER_TOKEN}`
		}
	};
	
	try {
		const res = await fetch(url, options)
		const data = await res.json();
		return data;
	} catch(e) {
		console.error(`Error ${e}`);
	}
}

export default getTrendiesMovies;