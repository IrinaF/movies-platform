const findPersonDetails = async (id: number) => {
	const url = `https://api.themoviedb.org/3/person/${id}?language=en-US`;
	const options = {
		method: 'GET',
		headers: {
			accept: 'application/json',
			Authorization: `Bearer ${process.env.API_BEARER_TOKEN}`
		}
	};
	
	try {
		const res = await fetch(url, options)
		const data = await res.json();
		return data;
	} catch(e) {
		console.error(`Error ${e}`);
	}
}

export default findPersonDetails;