import React, { FC } from "react";
import iconTwitter from "../../../assets/social-icons/icon-twitter.svg";
import iconFacebook from "../../../assets/social-icons/icon-facebook.svg";
import iconInstagram from "../../../assets/social-icons/icon-instagram.svg";
import iconTikTok from "../../../assets/social-icons/icon-tiktok.svg";
import iconYouTube from "../../../assets/social-icons/icon-youtube.svg";
import Image from "next/image";
import { socialLinks } from "@/types/types";

const socials = [
	{ name: "Twitter", url: "https://twitter.com/", icon: iconTwitter.src, id: "twitter_id" },
	{ name: "Facebook", url: "https://www.facebook.com/", icon: iconFacebook.src, id: "facebook_id" },
	{ name: "Instagram", url: "https://www.instagram.com/", icon: iconInstagram.src, id: "instagram_id" },
	{ name: "TikTok", url: "https://www.tiktok.com/", icon: iconTikTok.src, id: "tiktok_id" },
	{ name: "YouTube", url: "https://www.youtube.com/", icon: iconYouTube.src, id: "youtube_id" },
];

type SocialMediaLinksType = {
	socialLinks: socialLinks
}
const SocialMediaLinks: FC<SocialMediaLinksType> = ({socialLinks}) => {
	return (
		<>
			<ul className="flex gap-3 my-3">
				{ socials.filter(elem => (socialLinks as Record<string, string>)[elem.id]).map(elem =>
					<li key={elem.name}>
						<a href={`${elem.url}${(socialLinks as Record<string, string>)[elem.id]}`} target="_blank" rel="noreferrer">
							<Image width={30} height={30} src={elem.icon} alt={elem.name} />
						</a>
					</li>)
				}
			</ul>
		</>
	);
};

export default SocialMediaLinks;