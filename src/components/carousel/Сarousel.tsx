"use client";
import { default as MultiCarousel } from "react-multi-carousel";
import "react-multi-carousel/lib/styles.css";
import React from "react";

const responsive = {
    superLargeDesktop: {
        // the naming can be any, depends on you.
        breakpoint: { max: 4000, min: 3000 },
        items: 8
    },
    desktop: {
        breakpoint: { max: 3000, min: 1024 },
        items: 5
    },
    tablet: {
        breakpoint: { max: 1024, min: 464 },
        items: 3
    },
    mobile: {
        breakpoint: { max: 464, min: 0 },
        items: 1
    }
};

export default function Сarousel ({
    children,
}: Readonly<{
    children: React.ReactNode;
}>) {
    return (
        <MultiCarousel responsive={responsive}>
            {children}
        </MultiCarousel>
    );
}