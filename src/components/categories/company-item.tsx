import React, { FC } from "react";
import Image from "next/image";

type CompanyItemType = {
	id: number;
	name: string;
	logoPath?: string;
	originCountry?: string
}
const CompanyItem: FC<CompanyItemType> = ({id, name, logoPath, originCountry}) => {
	return (
		<div key={id} className="flex flex-row gap-5 items-center border-b-2 py-2">
			{
				logoPath ? <Image
					src={`https://image.tmdb.org/t/p/w154${logoPath}`}
					width={95}
					height={30}
					className="w-auto max-h-[30px]"
					alt="company-logo"
				/>
				: null
			}
			<span>{name}</span>
			{originCountry ? <span className="bg-slate-400 px-1 rounded text-white">({originCountry})</span> : ''}
		</div>
	);
};

export default CompanyItem