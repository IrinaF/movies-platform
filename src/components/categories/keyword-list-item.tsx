import React, { FC } from "react";
const KeywordListItem: FC<{id: number, name: string}> = ({id, name}) => {
	return (<p key={id}>{name}</p>);
};
export default KeywordListItem;