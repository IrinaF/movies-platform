import React, {FC} from "react";
import Image from "next/image";
import posterMovie from "../../../assets/poster-movie.jpg";
import {Card, CardBody} from "@nextui-org/card";
import {Link} from "@nextui-org/link";

type MovieCardType = {
	profilePath: string;
	name: string;
	knownFor: [{ id: number, name: string, title: string }];
	originalName?: string;
	knownForDepartment: string;
	id: number;
}
const PeopleCard: FC<MovieCardType> =
	({profilePath, name, knownForDepartment, originalName, knownFor, id}) => {
	return (
		<Card className="mb-5">
			<CardBody className="flex flex-row gap-5 items-center">
				<Link href={`/person/${name}-${id}`}>
					{profilePath
						? <Image
							src={`https://image.tmdb.org/t/p/w154${profilePath}`}
							width={95}
							height={140}
							className="w-auto"
							alt={`${posterMovie.src}`}
						/>
						: <span className="image-placeholder">🥸</span>}
				</Link>
				<div>
					<p className="font-bold">{name} <span className="text-grey">({originalName})</span></p>
					<p className="text-slate-500 font-bold">{knownForDepartment}:
						{knownFor.map((elem, ind) => <span key={elem.id} className="font-normal text-black ml-1">
							{elem.title} {elem.name}
							{ind === knownFor.length - 1 ? '' : ', '}
						</span>)}
					</p>
				
				</div>
			</CardBody>
		</Card>
	);
}

export default PeopleCard;