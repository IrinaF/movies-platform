import React, { FC } from "react";
import Image from "next/image";
import posterMovie from "../../../assets/poster-movie.jpg";
import {Card, CardBody} from "@nextui-org/card";
import {Link} from "@nextui-org/link";

type MovieCardType = {
	posterPath: string;
	title: string;
	realeaseDate: string;
	overview: string;
	originalTitle?: string;
	id: number;
}
const TvCard: FC<MovieCardType> = ({posterPath, title, realeaseDate, overview, originalTitle, id}) => {
	return (
		<Card className="mb-5">
			<CardBody className="flex flex-row gap-5 items-center">
				<Link className="flex-none" href={`/tv/${title}-${id}`}>
					{posterPath
						?  <Image
							src={`https://image.tmdb.org/t/p/w154${posterPath}`}
							width={95}
							height={140}
							className="w-auto"
							alt={`${posterMovie.src}`}
						/>
						: <span className="image-placeholder">📺</span>}
				</Link>
				<div>
					<p className="font-bold">{title} {originalTitle ? <span className="text-grey">({originalTitle})</span> : ''}</p>
					<p className="text-slate-500">{realeaseDate}</p>
					<p className="truncated-txt mt-1">{overview}</p>
				
				</div>
			</CardBody>
		</Card>
	);
}

export default TvCard;