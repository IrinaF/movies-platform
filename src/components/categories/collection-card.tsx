import React, { FC } from "react";
import Image from "next/image";
import posterMovie from "../../../assets/poster-movie.jpg";
import {Card, CardBody} from "@nextui-org/card";

type MovieCardType = {
	posterPath: string;
	name: string;
	overview: string;
	originalName?: string;
}
const CollectionCard: FC<MovieCardType> = ({posterPath, name, originalName, overview}) => {
	return (
		<Card className="mb-5">
			<CardBody className="flex flex-row gap-5 items-center">
				{posterPath
					?  <Image
						src={`https://image.tmdb.org/t/p/w154${posterPath}`}
						width={95}
						height={140}
						className="w-auto"
						alt={`${posterMovie.src}`}
					/>
					: <span className="image-placeholder">📺</span>}
				<div>
					<p className="font-bold">{name} {originalName ? <span className="text-grey">({originalName})</span> : ''}</p>
					<p className="truncated-txt mt-1">{overview}</p>
				</div>
			</CardBody>
		</Card>
	);
}

export default CollectionCard;