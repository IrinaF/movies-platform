import {Switch as NextUISwitch} from "@nextui-org/switch";

export default function Switch() {
    return (
        <NextUISwitch
            size="lg"
            color="secondary"
            checked
        />
    );
}