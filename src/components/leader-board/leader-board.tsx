import { FC } from "react";
import {Avatar} from "@nextui-org/avatar";
import {Progress} from "@nextui-org/react";

type User = {
	name: string;
	progressAllTime: number;
	progressWeek: number;
  };
  
  type LeaderBoardProps = {
	users: User[];
  };

  const LeaderBoard: FC<LeaderBoardProps> = ({ users }) => {
	return (
		<div className="mt-10">
			<div className="flex gap-7 mb-7">
				<h2 className="text-2xl font-bold">Leaderboard</h2>
				<div>
					<div className="flex gap-2 items-center">
						<span className="block w-2.5 h-2.5 rounded-full bg-primary"></span>
						<p className="text-base">All Time Edits</p>
					</div>
					<div className="flex gap-2 items-center">
						<span className="block w-2.5 h-2.5 rounded-full bg-success"></span>
						<p className="text-base">Edits This Week</p>
					</div>
				</div>
			</div>
			<div>
				<div className="flex flex-wrap">
					{users.map((user: User) => (
						<div className="flex md:basis-2/4 basis-full gap-4 pr-4 mb-7" key={user.name}>
							<Avatar
								name={user.name}
								size="lg"
							/>
							<div className="flex-auto">
								<p>{user.name}</p>
								<Progress
									className="mb-2"
									color="primary"
									aria-label="Leaderboard edits"
									value={user.progressAllTime}
								/>
								<Progress
									color="success"
									aria-label="Leaderboard edits"
									value={user.progressWeek}
								/>
							</div>
						</div>
					))}
				</div>
			</div>
		</div>
	);
};

export default LeaderBoard;