import { FC } from "react";
import Link from "next/link";
import footerIcon from "../../../assets/footer-icon.jpg";
import Image from 'next/image'

const footerLinks = [
	{ label: "Contact us", href: "/contact-us" },
	{ label: "Terms of Service", href: "/terms-of-service" },
	{ label: "Privacy Policy", href: "/privacy-policy" },
];

const Footer: FC = () => {
	return (
		<footer className="bg-slate-800 text-white p-8">
			<div className="container mx-auto max-w-5xl">
				<nav className="flex gap-7 items-center">
					<Image
						src={`${footerIcon.src}`}
						width={80}
						height={80}
						alt={`${footerIcon.src}`}
					/>
					<ul className="flex flex-auto justify-between">
						{footerLinks.map((elem) => (
							<li key={elem.label}>
								<Link href={elem.href}>{elem.label}</Link>
							</li>
						))}
					</ul>
				</nav>
			</div>
		</footer>
	);
};

export default Footer