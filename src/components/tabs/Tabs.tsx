import React, {FC} from "react";
import {Tab as NextUITab, Tabs as NextUITabs} from "@nextui-org/react";
import Carousel from "@/components/carousel/Сarousel";
import Image from "next/image";
import {Link} from "@nextui-org/link";
import {CircularProgress} from "@nextui-org/react";

type TabsType = {
	tabs: { title: string; key: string, }[];
	onSelectionChange: (key: string | number) => void;
	data: { id: number, title?: string, original_title: string, release_date?: string,
		vote_average: number, poster_path: string, name?: string, first_air_date?: string, media_type: string }[];
	tabTitle: string;
}

const Tabs: FC<TabsType> = (
	{
		tabs,
		onSelectionChange,
		data,
		tabTitle
	}) => {
	return (
		<div className="mb-8">
			<p className="text-2xl inline mr-3">{tabTitle}</p>
			<NextUITabs onSelectionChange={onSelectionChange} radius="full" classNames={{
				tabList: "border-1 relative p-0 bg-white",
				cursor: "w-full bg-indigo-950",
				tab: "max-w-full min-w-20 px-5 h-8",
				tabContent: "text-gradient",
			}}>
				{tabs.map((tab) => (
					<NextUITab title={tab.title} key={tab.key}>
						<Carousel>
							{data.map((i) => (
								<div
									className="min-h-52 pr-3"
									key={i.id}
								>
									<div className="relative">
										<Link className="flex-none" href={`/${i.media_type ? i.media_type : tab.key}/${i.title ? i.title : i.name}-${i.id}`}>
											{i.poster_path
												? <Image
													src={`https://image.tmdb.org/t/p/w342${i.poster_path}`}
													width={95}
													height={140}
													className="w-auto rounded-lg"
													alt="poster"
												/>
												: <span className="image-placeholder">📽</span>}
											{i.vote_average ?
												<CircularProgress
													aria-label="User rating"
													size="md"
													value={i.vote_average * 10}
													color="success"
													className="bg-black rounded-full text-white absolute bottom-[-15px] left-1"
													strokeWidth={3}
													showValueLabel={true}
												/>
												: null}
										</Link>
									</div>
									<p className="font-bold pt-2">{i.title ? i.title : i.name}</p>
									<p className="text-slate-500">{i.release_date ? i.release_date : i.first_air_date}</p>
								</div>
							))}
						</Carousel>
					</NextUITab>
				))}
			</NextUITabs>
		</div>
	);
}
export default Tabs;
