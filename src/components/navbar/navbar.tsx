import React, { FC } from "react";
import { Navbar as NextUINavbar, NavbarItem as NextUINavbarItem } from "@nextui-org/react";
import Link from "next/link";
import classes from "./navbar.module.css";

const navbarLinks = [
	{ label: "Home", href: "/" },
	{ label: "Search", href: "/search?category=movie" },
];

const Navbar: FC<{}> = ({}) => {
	return (
		<NextUINavbar className={classes.navbar} isBordered>
			{navbarLinks.map((elem) => (
				<NextUINavbarItem key={elem.label}>
					<Link href={elem.href}>{elem.label}</Link>
				</NextUINavbarItem>
			))}
		</NextUINavbar>
	);
};

export default Navbar;
